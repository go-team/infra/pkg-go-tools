FROM golang:1.16.6-alpine3.14 as builder

WORKDIR /app
COPY . .
RUN go build -v salsa.debian.org/go-team/infra/pkg-go-tools/cmd/pgt-api-server

FROM alpine:3.14.0

LABEL maintainer="Aloïs Micard <creekorful@debian.org>"

# So that we can run as unprivileged user inside the container.
RUN echo 'nobody:x:99:99:nobody:/:/bin/sh' >> /etc/passwd
USER nobody

COPY --from=builder /app/pgt-api-server /usr/bin/pgt-api-server

EXPOSE 8080

ENTRYPOINT ["/usr/bin/pgt-api-server", "-listen=:8080"]
