#!/bin/bash

# Abort if anything goes wrong
set -e

usage() {
    echo \
"Usage: $0 [options] <group or project path>

$(basename $0) is uses the GitLab API to enable shared instance runners in Salsa
for a single project, or for all projects in a group namespace (team).

This script is designed to be self-contained with minimal dependencies on Bash,
Glab (Go) and jq.

  Options:
    --debug         Output extra debugging information

  Example:
    $0 go-team/glow # Enable for a single project
    $0 go-team      # Enable in all projects in team
"
}

while :
do
  case "$1" in
    --debug)
      DEBUG=1
      shift
      ;;
    -h | --help)
      usage
      exit 0
      ;;
    --)
      # No more options
      shift
      break
      ;;
    -*)
      echo "ERROR: Unknown option: $1" >&2
      ## or call function display_help
      exit 1
      ;;
    '')
      # No more options or arguments, stop processing
      break
      ;;
    *)
      export GITLAB_PATH="$1"
      shift
      ;;
  esac
done

# If no arguments given, just show usage
if [ -z "$GITLAB_PATH" ]
then
  echo "ERROR: A group or project must be passed as argument"
  usage
  exit 0
fi

# Dependency check
if ! command -v jq &> /dev/null
then
  echo "ERROR: jq not found"
  exit 1
fi

# Dependency check
if ! command -v glab &> /dev/null
then
  echo "ERROR: glab not found"
  exit 1
fi

if [ -z "$GITLAB_TOKEN" ]
then
  echo "ERROR: To use glab, a GITLAB_TOKEN environment variable must be set"
  echo "Create on at https://salsa.debian.org/-/user_settings/personal_access_tokens"
  echo "with permissions 'api' and 'write_repository'"
  exit 1
fi

# This script is intended specifically to interact with Salsa
export GITLAB_HOST=https://salsa.debian.org

process_project() {
  local project_path="$1"
  local project_name="${project_path##*/}"
  local status=""

  # Url encode
  project_path_urlencoded=$(echo -n "$project_path" | jq -sRr @uri)

  # Check if setting is already set
  PROJECT_SETTINGS="$(glab api "projects/${project_path_urlencoded}")"

  if [ -n "$DEBUG" ]
  then
    echo "DEBUG: glab api 'projects/${project_path_urlencoded}'"
    echo "DEBUG: PROJECT_SETTINGS => ${PROJECT_SETTINGS}" | cut -c -300
  fi

  if [[ "$PROJECT_SETTINGS" == '{"error":"'* ]]
  then
    echo "ERROR: API returned for project: $PROJECT_SETTINGS"
    exit 1
  elif [[ "$PROJECT_SETTINGS" == '{"message":"404 Project Not Found"}' ]]
  then
    echo "ERROR: Project 'projects/${project_path_urlencoded}' not found!"
    exit 1
  fi

  if echo "$PROJECT_SETTINGS" | grep -q '"shared_runners_enabled":true'
  then
    status="✅ Instance runners already enabled"
  else
    # https://docs.gitlab.com/ee/api/projects.html#edit-a-project
    UPDATE_PROJECT="$(glab api "projects/${project_path_urlencoded}" \
        -X PUT \
        -f shared_runners_enabled="true" > /dev/null)"

    if [ -n "$DEBUG" ]
    then
      echo "DEBUG: UPDATE_PROJECT => ${UPDATE_PROJECT}"
    fi

    if [[ "$UPDATE_PROJECT" == '{"error":"'* ]]
    then
      echo "ERROR: API returned for project modification: $PROJECT_SETTINGS"
      exit 1
    fi

    # Checking status like this on the next line is not reliable
    if echo $UPDATE_PROJECT | grep -q error
    then
      status="❌ Failed to activate"
    else
      status="⏩ Instance runners enabled now"
    fi
  fi

  printf "%-60s %s\n" "$project_path" "$status"
}

# Check that user has can list their own access tokens. Pass any common project
# name so glab immediately does the check instead of guessing project names.
_=$(glab token list --repo debian/entr --user @me)
# If glab yields any error messages (network connection issues, 401 unauthorized
# replies from API) they will be visible to user, and if there is an exit code,
# script execution will stop here.

# Check if this is a group by trying to list projects
echo "Fetching settings of ${GITLAB_HOST}/${GITLAB_PATH}"

# Using the regular API would return thousands of lines of JSON output with all
# settings of all projects in a group. Instead, craft a GraphQL query to only
# get the list of project names.
GRAPHQL_QUERY="glab api graphql --paginate -f query='
  query(\$endCursor: String) {
    group(fullPath: \"$GITLAB_PATH\") {
      name
      projects(first: 300, after: \$endCursor) {
        edges {
         node {
           name
           fullPath
         }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
'"

if [ -n "$DEBUG" ]
then
  echo "DEBUG: $GRAPHQL_QUERY"
fi

# Using eval is not dangerous here as ut runs with the same permissions a user
# already has
GROUP_PROJECTS="$(eval $GRAPHQL_QUERY)"

if [ -n "$DEBUG" ]
then
  echo "DEBUG: GROUP_PROJECTS => $GROUP_PROJECTS" | cut -c -300
fi

echo "======================================================================="
printf "%-60s %s\n" "PROJECT" "STATUS"
echo "-----------------------------------------------------------------------"

if [[ "$GROUP_PROJECTS" == '{"data":{"group":null}}' ]]
then
  # Try if a single project can be found at the path
  process_project "$GITLAB_PATH"
else
  # Query returned some results, thus assume this is a group and try processing
  # each project in the group
  echo "$GROUP_PROJECTS" | jq -r '.data.group.projects.edges.[].node.fullPath' | while read -r project_path
  do
    process_project "$project_path"
  done
fi
