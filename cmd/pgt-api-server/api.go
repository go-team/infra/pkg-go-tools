// Binary pgt-api-server exposes functionality for use by Debian go-team members.
package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
)

var (
	listen = flag.String("listen",
		":4039",
		"[host]:port to listen on")


	repoCreation = flag.Bool("repo_creation",
		true,
		"Whether /v1/createrepo is enabled. Can be flipped quickly to limit abuse, should it happen.")
)

const group = "go-team/packages"

var salsa = mustSalsaClient()

func mustSalsaClient() *gitlab.Client {
	cl, err := gitlab.NewClient(os.Getenv("SALSA_TOKEN"), gitlab.WithBaseURL("https://salsa.debian.org/api/v4"))
	if err != nil {
		panic(err)
	}

	return cl
}

var rateLimit = make(chan struct{})

// internalServerError returns a non-nil error from handler as a HTTP 500 error.
func internalServerError(handler func(http.ResponseWriter, *http.Request) error) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := handler(w, r); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
}

// TODO: misnomer: rename or make an actual apache log
func apacheLog(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		src := r.Header.Get("X-Forwarded-For")
		if src == "" ||
			(!strings.HasPrefix(r.RemoteAddr, "[::1]:") &&
				!strings.HasPrefix(r.RemoteAddr, "127.0.0.1:") &&
				!strings.HasPrefix(r.RemoteAddr, "172.17.")) {
			src = r.RemoteAddr
		}

		start := time.Now()
		h.ServeHTTP(w, r)
		log.Printf("%s %s %s: %v", src, r.Method, r.URL, time.Since(start))
	})
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()

	go func() {
		for range time.Tick(1 * time.Second) {
			rateLimit <- struct{}{}
		}
	}()

	// TODO: prometheus metrics

	http.Handle("/v1/createrepo", apacheLog(internalServerError(createRepo)))
	http.Handle("/v1/configrepo", apacheLog(internalServerError(configRepo)))
	http.Handle("/health", apacheLog(internalServerError(health)))

	log.Printf("listening on %s", *listen)
	http.ListenAndServe(*listen, nil)
}
