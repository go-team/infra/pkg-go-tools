package main

import (
	"net/http"
)

func health(w http.ResponseWriter, r *http.Request) error {
	if r.Method != "GET" {
		http.Error(w, "this URL requires HTTP POST", http.StatusMethodNotAllowed)
		return nil
	}

	w.WriteHeader(200)
	return nil
}
