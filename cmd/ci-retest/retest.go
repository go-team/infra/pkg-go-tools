// Binary ci-retest programmatically trigger CI tests for all go-team
// packaging repositories.
package main

import (
	"flag"
	"github.com/xanzy/go-gitlab"
	"log"
	"os"
)

const group = "go-team/packages"

var salsa = mustSalsaClient()

func mustSalsaClient() *gitlab.Client {
	cl, err := gitlab.NewClient(os.Getenv("SALSA_TOKEN"), gitlab.WithBaseURL("https://salsa.debian.org/api/v4"))
	if err != nil {
		panic(err)
	}

	return cl
}

func main() {
	pageFlag := flag.Int("page", 1, "starting project page")
	projectFlag := flag.Int("project", -1, "starting project ID")

	flag.Parse()

	orderBy := "id"
	sort := "asc"

	page := *pageFlag
	for {
		projects, resp, err := salsa.Groups.ListGroupProjects(group, &gitlab.ListGroupProjectsOptions{
			OrderBy:     &orderBy,
			Sort:        &sort,
			ListOptions: gitlab.ListOptions{Page: page, PerPage: 100},
		})
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Processing page #%d (%d projects)", page, len(projects))

		if *projectFlag == -1 {
			*projectFlag = projects[0].ID
		}

		for _, project := range projects {
			if *projectFlag > project.ID {
				log.Printf("Skipping #%d %s", project.ID, project.Name)
				continue
			}

			if project.BuildsAccessLevel == gitlab.DisabledAccessControl {
				log.Printf("Skipping project #%d %s (pipelines disabled)", project.ID, project.Name)
				continue
			}

			log.Printf("Triggering pipeline for #%d %s", project.ID, project.Name)

			ref := project.DefaultBranch

			_, _, err := salsa.Pipelines.CreatePipeline(project.ID, &gitlab.CreatePipelineOptions{
				Ref: &ref,
			})
			if err != nil {
				log.Fatalf("error while triggering pipeline for %s: %s", project.Name, err)
			}
		}

		if page == resp.TotalPages {
			break
		}
		page = resp.NextPage
	}
}
