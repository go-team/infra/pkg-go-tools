// Binary ci-config programmatically configures CI settings for all go-team
// packaging repositories.
package main

import (
	"flag"
	"log"
	"os"
	"salsa.debian.org/go-team/infra/pkg-go-tools/config"

	"github.com/xanzy/go-gitlab"
)

const group = "go-team/packages"

var salsa = mustSalsaClient()

func mustSalsaClient() *gitlab.Client {
	cl, err := gitlab.NewClient(os.Getenv("SALSA_TOKEN"), gitlab.WithBaseURL("https://salsa.debian.org/api/v4"))
	if err != nil {
		panic(err)
	}

	return cl
}

func main() {
	pageFlag := flag.Int("page", 1, "starting project page")

	flag.Parse()

	page := *pageFlag
	for {
		projects, resp, err := salsa.Groups.ListGroupProjects(group, &gitlab.ListGroupProjectsOptions{
			ListOptions: gitlab.ListOptions{Page: page, PerPage: 100},
		})
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Processing page #%d (%d projects)", page, len(projects))

		for _, project := range projects {
			if err := config.All(project); err != nil {
				log.Fatal(err)
			}
		}

		if page == resp.TotalPages {
			break
		}
		page = resp.NextPage
	}
}
