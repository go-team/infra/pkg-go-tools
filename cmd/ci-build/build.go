// Binary ci-build locates all Debian-packaged Go packages in a GOPATH, builds
// them, and persists the result. This is used for continuous integration on
// salsa in combination with an overlay for before/after comparisons.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"syscall"

	"pault.ag/go/debian/control"
)

var (
	dryRun = flag.Bool("dry_run",
		false,
		"Print go tool command lines instead of actually running commands (for development)")

	verbose = flag.Bool("verbose",
		false,
		"Display output of `go` calls")

	parallelExamine = flag.Int("parallel_examine",
		20,
		"Parallelism level for examining packages (finding the Go packages to build)")

	parallelBuild = flag.Int("parallel_build",
		16,
		"Parallelism level for building packages")

	unprivilegedUid = flag.Int("unprivileged_uid",
		1000,
		"system uid (id -u) of an unprivileged user to drop privileges to after setting up the build environment")

	unprivilegedGid = flag.Int("unprivileged_gid",
		1000,
		"system gid (id -g) of an unprivileged user to drop privileges to after setting up the build environment")

	exemptionsPath = flag.String("exemptions",
		"",
		"Path to an exemptions .json file") // TODO: describe format

	examineCache = flag.String("examine_cache",
		"/cache/examine",
		"Path in which to store JSON cache files with Debian package examination results (build targets, etc.)")
)

func goEnv() []string {
	return []string{
		"GOPATH=" + os.Getenv("GOPATH"),
		"GOCACHE=" + os.Getenv("GOCACHE"),
		"GO111MODULE=off",
		"PATH=/usr/bin:/bin:/usr/games:/usr/sbin:/sbin",
	}
}

func isDir(err error) bool {
	if pe, ok := err.(*os.PathError); ok {
		return pe.Err == syscall.EISDIR
	}
	return false
}

// goImportPath returns the machine-readable XS-Go-Import-Path from the
// debian/control file located at controlPath.
func goImportPath(s control.SourceParagraph) string {
	for key, val := range s.Values {
		if strings.ToLower(key) == "xs-go-import-path" {
			paths := strings.Split(val, ",")
			return strings.TrimSuffix(paths[0], "/")
		}
	}
	return ""
}

var varRe = regexp.MustCompile(`^([^ ]+) :?= (.*)$`)

func buildTargets(packagingDir, importPath string) ([]string, bool, bool, error) {
	disableTests := false
	// Read DH_GO* variables from debian/rules Makefile:
	mk := exec.Command("make", "-Rrnpf", "debian/rules", "dh")
	mk.Dir = packagingDir
	mk.Env = []string{"LC_ALL=C"}
	mk.Stderr = os.Stderr
	out, err := mk.Output()
	if err != nil {
		return nil, false, false, err
	}
	vars := make(map[string]string)
	vars["DH_GOPKG"] = ""
	vars["DH_GOLANG_BUILDPKG"] = ""
	vars["DH_GOLANG_EXCLUDES"] = ""
	vars["DH_GOLANG_GO_GENERATE"] = ""
	vars["DEB_BUILD_OPTIONS"] = ""
	for _, line := range strings.Split(strings.TrimSpace(string(out)), "\n") {
		if !strings.HasPrefix(line, "DH_GO") &&
			!strings.HasPrefix(line, "override_dh_auto_test:") &&
			!strings.HasPrefix(line, "DEB_BUILD_OPTIONS") {
			continue // optimization: skip variables we are not interested in
		}
		if strings.HasPrefix(line, "override_dh_auto_test:") {
			disableTests = true
		}
		matches := varRe.FindStringSubmatch(line)
		if matches == nil {
			continue
		}
		if _, ok := vars[matches[1]]; !ok {
			continue // variable not in map: not interested
		}
		vars[matches[1]] = matches[2]
	}

	// Figure out what to build, just like dh-golang:
	gopkg := vars["DH_GOPKG"]
	if gopkg == "" {
		gopkg = importPath
	}
	buildpkg := vars["DH_GOLANG_BUILDPKG"]
	if buildpkg == "" {
		buildpkg = gopkg + "/..."
	}
	generate := vars["DH_GOLANG_GO_GENERATE"] == "1"
	excludes := vars["DH_GOLANG_EXCLUDES"]
	if excludes == "" {
		// TODO(speed): cannot return early because of (e.g.?)
		// golang-github-sjoerdsimons-ostree-go, which contains packages that
		// cannot be loaded (empty files). The “go list” step will filter those
		// out.
		// return []string{buildpkg}, generate, disableTests, nil
	}

	if vars["DEB_BUILD_OPTIONS"] == "nocheck" {
		disableTests = true
	}

	// TODO: put that go list call into dh-golang, too, then remove the workaround from gogoprotobuf
	list := exec.Command("go", "list", "-f", `{{if or (ne (join .GoFiles "") "") (ne (join .CgoFiles "") "")}}{{.ImportPath}}{{end}}`, buildpkg)
	list.Env = goEnv()
	list.Stderr = os.Stderr

	if *verbose {
		log.Printf("executing: %v", list.Args)
	}

	target, err := list.Output()
	if err != nil {
		return nil, false, false, err
	}
	targets := strings.Split(strings.TrimSpace(string(target)), "\n")

	if excludes == "" {
		return targets, generate, disableTests, nil
	}

	for _, pattern := range strings.Split(excludes, " ") {
		// TODO(correctness): use PCRE to better approximate Perl regexps
		re, err := regexp.Compile(pattern)
		if err != nil {
			log.Printf("invalid DH_GOLANG_EXCLUDES regexp %q: %v", pattern, err)
			continue
		}
		var filtered []string
		for _, t := range targets {
			if re.MatchString(t) {
				continue
			}
			filtered = append(filtered, t)
		}
		targets = filtered
	}

	return targets, generate, disableTests, nil
}

type pkg struct {
	Source       string   `json:"source,omitempty"`     // e.g. golang-golang-x-oauth2
	Dir          string   `json:"dir,omitempty"`        // e.g. /srv/gopath/src/xi2.org/x/xz
	ImportPath   string   `json:"importpath,omitempty"` // e.g. xi2.org/x/xz
	Targets      []string `json:"targets,omitempty"`    // e.g. xi2.org/x/xz/...
	Generate     bool     `json:"generate,omitempty"`   // call go generate on targets before building?
	DisableTests bool     `json:"disabletests,omitempty"`
}

func (p *pkg) cachePath() (string, error) {
	b, err := ioutil.ReadFile(filepath.Join(p.Dir, "debian", ".hashes"))
	if err != nil {
		return "", err
	}
	h := fnv.New32()
	h.Write(b)
	cacheKey := fmt.Sprintf("%x", h.Sum(nil))
	return filepath.Join(*examineCache, cacheKey[:2], cacheKey+".json"), nil
}

func (p *pkg) loadCached(cachePath string) (bool, error) {
	b, err := ioutil.ReadFile(cachePath)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	if err := json.Unmarshal(b, p); err != nil {
		return false, err
	}
	return true, nil
}

func (p *pkg) storeCache(cachePath string) error {
	b, err := json.Marshal(&pkg{
		Targets:      p.Targets,
		ImportPath:   p.ImportPath,
		Generate:     p.Generate,
		DisableTests: p.DisableTests,
	})
	if err != nil {
		return err
	}
	if err := os.MkdirAll(filepath.Dir(cachePath), 0755); err != nil {
		return err
	}
	return ioutil.WriteFile(cachePath, b, 0644)
}

func (p *pkg) examine() error {
	cachePath, err := p.cachePath()
	if err != nil {
		return err
	}

	cached, err := p.loadCached(cachePath)
	if err != nil {
		return err
	}
	if cached {
		return nil
	}

	p.Targets, p.Generate, p.DisableTests, err = buildTargets(p.Dir, p.ImportPath)
	if err != nil {
		return err
	}

	if err := p.storeCache(cachePath); err != nil {
		log.Printf("could not persist examine cache entry %s: %v", cachePath, err)
	}

	return nil
}

type exemptions struct {
	// Exemptions maps from importpath (e.g. xi2.org/x/xz) to bug URL
	// (e.g. https://bugs.debian.org/123456, or https://github.com/foo/bar/issues/3).
	Exemptions map[string]string `json:"exemptions"`
}

func locate() ([]pkg, error) {
	var ex exemptions
	if *exemptionsPath != "" {
		b, err := ioutil.ReadFile(*exemptionsPath)
		if err != nil {
			return nil, err
		}
		if err := json.Unmarshal(b, &ex); err != nil {
			return nil, err
		}
		log.Printf("%d exemptions loaded from %s", len(ex.Exemptions), *exemptionsPath)
	}
	toExamine := make(chan pkg)
	var (
		wg      sync.WaitGroup
		mu      sync.Mutex
		targets []pkg
	)

	for i := 0; i < *parallelExamine; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for p := range toExamine {
				if err := p.examine(); err != nil {
					log.Printf("could not find build targets for %s: %v", p.Dir, err)
					continue
				}
				mu.Lock()
				targets = append(targets, p)
				mu.Unlock()
			}
		}()
	}

	dir, err := filepath.EvalSymlinks(filepath.Join(os.Getenv("GOPATH"), "src"))
	if err != nil {
		return nil, err
	}

	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err // stop on walk errors
		}
		if info == nil || !info.IsDir() {
			return nil // skip non-directories
		}
		if info.Name() != "debian" {
			return nil
		}
		ctrl, err := control.ParseControlFile(filepath.Join(path, "control"))
		if err != nil {
			if os.IsNotExist(err) || isDir(err) {
				// debian/control is either not found or is a directory (e.g pault.ag/go/debian/control/)
				return nil // debian/control not found, skip directory
			}

			// Some packages may have malformed debian/control file,
			// so we report them but do not fail the build
			//
			// example: github.com/containers/skopeo
			log.Printf("SKIP %s: %v", filepath.Join(path, "control"), err)
			return nil
		}
		// Defense in depth: verify we are dealing with a Debian package (and
		// not, for example, test data) by checking the XS-Go-Import-Path
		// matches the file location.
		importPath := goImportPath(ctrl.Source)
		if importPath == "" {
			return nil
		}
		toplevel := filepath.Dir(path)
		if !strings.HasSuffix(toplevel, importPath) {
			log.Printf("%q does not have suffix %q", toplevel, importPath)
			return nil
		}
		if u, ok := ex.Exemptions[importPath]; ok {
			log.Printf("SKIP %s: %s", importPath, u)
			return nil
		}
		toExamine <- pkg{Source: ctrl.Source.Source, Dir: toplevel, ImportPath: importPath}
		return nil
	})
	if err != nil {
		return nil, err
	}

	close(toExamine)
	wg.Wait()

	return targets, nil
}

type result struct {
	Pkg pkg    `json:"pkg"`
	Out string `json:"output"`
	Err string `json:"error"`
}

func generate(p pkg) (string, error) {
	if !p.Generate {
		return "", nil
	}
	g := exec.Command("go", append([]string{"generate"}, p.Targets...)...)
	g.Env = goEnv()
	if *dryRun {
		log.Println(g.Args)
		return "", nil
	}

	out, err := g.CombinedOutput()
	if *verbose {
		log.Printf("executing: %v: %s", g.Args, string(out))
	}

	return string(out), err
}

func build(p pkg) (string, error) {
	if out, err := generate(p); err != nil {
		return out, err
	}

	var t *exec.Cmd
	if p.DisableTests {
		log.Printf("build only (Debian packaging disables tests): %s", p.ImportPath)

		t = exec.Command("go", append([]string{"build"}, p.Targets...)...)
	} else {
		// -vet=off because many packages across debian have vet issues
		t = exec.Command("go", append([]string{"test", "-timeout=3m", "-vet=off"}, p.Targets...)...)
	}
	t.Env = goEnv()
	if *dryRun {
		log.Println(t.Args)
		return "", nil
	}

	out, err := t.CombinedOutput()
	if *verbose {
		log.Printf("executing: %v: %s", t.Args, string(out))
	}

	return string(out), err
}

func buildAll(targets []pkg) error {
	var (
		toBuild = make(chan pkg)
		wg      sync.WaitGroup
		mu      sync.Mutex
		results = make(map[string]result)
	)

	for i := 0; i < *parallelBuild; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for p := range toBuild {
				out, err := build(p)
				mu.Lock()
				res := result{
					Pkg: p,
					Out: out,
				}
				if err != nil {
					res.Err = err.Error()
				}
				results[p.ImportPath] = res
				mu.Unlock()
			}
		}()
	}

	for _, t := range targets {
		toBuild <- t
	}

	close(toBuild)
	wg.Wait()

	return json.NewEncoder(os.Stdout).Encode(results)
}

func logic() error {
	targets, err := locate()
	if err != nil {
		return err
	}

	// Build packages (in parallel). We cannot shell out to go(1) and xargs(1)
	// for this, as we need to capture the exit codes of individual targets in
	// order to compare them later.
	return buildAll(targets)
}

func setup() error {
	// Create a new network namespace so that there is no internet connectivity
	// during compilation/testing.
	if err := syscall.Unshare(syscall.CLONE_NEWNET); err != nil {
		return fmt.Errorf("creating network namespace: %v", err)
	}

	// Bring up the loopback interface in the new namespace:
	lo := exec.Command("/bin/ip", "link", "set", "dev", "lo", "up")
	lo.Stderr = os.Stderr
	if err := lo.Run(); err != nil {
		return err
	}

	// Execute this binary again, but as an unprivileged user:
	cmd := exec.Command(os.Args[0], os.Args[1:]...)
	cmd.Env = []string{
		"CI_BUILD_PRIVILEGES_DROPPED=1",
		"PATH=" + os.Getenv("PATH"),
		"GOPATH=" + os.Getenv("GOPATH"),
		"GOCACHE=" + os.Getenv("GOCACHE"),
	}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Credential: &syscall.Credential{
			Uid: uint32(*unprivilegedUid),
			Gid: uint32(*unprivilegedGid),
		},
	}
	return cmd.Run()
}

func main() {
	flag.Parse()

	for _, key := range []string{"GOPATH", "GOCACHE"} {
		if _, ok := os.LookupEnv(key); !ok {
			log.Fatalf("environment variable %s required, but not set in .gitlab-ci.yml?", key)
		}
	}

	if os.Getenv("CI_BUILD_PRIVILEGES_DROPPED") != "1" {
		if err := setup(); err != nil {
			log.Fatalf("%v, %T, %#v", err, err, err)
		}
		return
	}

	if err := logic(); err != nil {
		log.Fatalf("%v, %T, %#v", err, err, err)
	}
}
