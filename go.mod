module salsa.debian.org/go-team/infra/pkg-go-tools

go 1.16

require (
	github.com/xanzy/go-gitlab v0.50.1
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/tools v0.1.5
	pault.ag/go/archive v0.0.0-20200912011324-7149510a39c7
	pault.ag/go/blobstore v0.0.0-20180314122834-d6d187c5a029 // indirect
	pault.ag/go/debian v0.11.0
)
