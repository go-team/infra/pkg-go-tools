// Package config contains functions to configure GitLab projects for the Debian
// go-team.
package config

import (
	"log"
	"net/url"
	"os"
	"strings"

	gitlab "github.com/xanzy/go-gitlab"
)

// TODO(later): update dh-make-golang to be consistent with this code

const (
	ciFileName = "debian/gitlab-ci.yml"
)

const commitMsg = `[skip ci] update debian/gitlab-ci.yml (using pkg-go-tools/ci-config)

See: https://salsa.debian.org/go-team/infra/pkg-go-tools
Gbp-Dch: Ignore`

var salsa = mustSalsaClient()

func mustSalsaClient() *gitlab.Client {
	cl, err := gitlab.NewClient(os.Getenv("SALSA_TOKEN"), gitlab.WithBaseURL("https://salsa.debian.org/api/v4"))
	if err != nil {
		panic(err)
	}

	return cl
}

func All(p *gitlab.Project) error {
	if err := fixSettings(p); err != nil {
		return err
	}
	if err := writeGitlabCiYml(p); err != nil {
		return err
	}
	if err := fixWebhooks(p); err != nil {
		return err
	}
	if err := protectBranches(p); err != nil {
		return err
	}
	return nil
}

func fixSettings(p *gitlab.Project) error {
	change := false
	opt := &gitlab.EditProjectOptions{}
	if got, want := p.SharedRunnersEnabled, true; got != want {
		log.Printf("%s: enabling shared runners", p.PathWithNamespace)
		opt.SharedRunnersEnabled = gitlab.Bool(true)
		change = true
	}
	// See https://salsa.debian.org/go-team/ci/issues/1
	const maintainerOverride = "debian/.gitlab-ci.yml"
	// requires https://github.com/xanzy/go-gitlab/pull/306
	if got, want := p.CIConfigPath, ciFileName; got == "" || (got != want && got != maintainerOverride) {
		log.Printf("%s: setting ci config path from %q to %q", p.PathWithNamespace, got, want)
		opt.CIConfigPath = gitlab.String(ciFileName)
		change = true
	}
	if !change {
		return nil
	}

	if _, _, err := salsa.Projects.EditProject(p.ID, opt); err != nil {
		return err
	}
	return nil
}

func writeGitlabCiYml(p *gitlab.Project) error {
	branches, _, err := salsa.Branches.ListBranches(p.ID, &gitlab.ListBranchesOptions{})
	if err != nil {
		return err
	}
	for _, branch := range branches {
		if branch.Name != "master" && !strings.HasPrefix(branch.Name, "debian/") {
			continue
		}

		b, _, err := salsa.RepositoryFiles.GetRawFile(p.ID, ciFileName, &gitlab.GetRawFileOptions{
			Ref: gitlab.String(branch.Name),
		})
		if err != nil {
			if er, ok := err.(*gitlab.ErrorResponse); !ok || er.Response.StatusCode != 404 {
				return err
			}
			log.Printf("%s: creating debian/gitlab-ci.yml in branch %s", p.PathWithNamespace, branch.Name)
			if _, _, err := salsa.RepositoryFiles.CreateFile(p.ID, ciFileName, &gitlab.CreateFileOptions{
				Branch:        gitlab.String(branch.Name),
				Content:       gitlab.String(gitlabciymlTmpl),
				CommitMessage: gitlab.String(commitMsg),
			}); err != nil {
				return err
			}
			continue
		}
		if string(b) == gitlabciymlTmpl {
			log.Printf("%s: skipping up-to-date debian/gitlab-ci.yml in branch %s", p.PathWithNamespace, branch.Name)
			continue // up to date
		}
		log.Printf("%s: updating debian/gitlab-ci.yml in branch %s", p.PathWithNamespace, branch.Name)
		if _, _, err := salsa.RepositoryFiles.UpdateFile(p.ID, ciFileName, &gitlab.UpdateFileOptions{
			Branch:        gitlab.String(branch.Name),
			Content:       gitlab.String(gitlabciymlTmpl),
			CommitMessage: gitlab.String(commitMsg),
		}); err != nil {
			return err
		}
	}
	return nil
}

func maybeAddTagpending(p *gitlab.Project, hooks []*gitlab.ProjectHook) error {
	for _, hook := range hooks {
		if strings.HasPrefix(hook.URL, "https://webhook.salsa.debian.org/tagpending/") {
			return nil // hook already set up
		}
	}
	_, _, err := salsa.Projects.AddProjectHook(p.ID, &gitlab.AddProjectHookOptions{
		URL:        gitlab.String("https://webhook.salsa.debian.org/tagpending/" + p.Name),
		PushEvents: gitlab.Bool(true),
	})

	return err
}

func maybeAddKGB(p *gitlab.Project, hooks []*gitlab.ProjectHook) error {
	const kgbBase = "http://kgb.debian.net:9418/webhook/"
	u, _ := url.Parse(kgbBase)
	q := u.Query()
	q.Set("channel", "#debian-golang")
	u.RawQuery = q.Encode()

	for _, hook := range hooks {
		if strings.HasPrefix(hook.URL, kgbBase) {
			return nil // hook already set up
		}
	}
	_, _, err := salsa.Projects.AddProjectHook(p.ID, &gitlab.AddProjectHookOptions{
		URL:        gitlab.String(u.String()),
		PushEvents: gitlab.Bool(true),
	})

	return err
}

func fixWebhooks(p *gitlab.Project) error {
	hooks, _, err := salsa.Projects.ListProjectHooks(p.ID, &gitlab.ListProjectHooksOptions{})
	if err != nil {
		return err
	}

	if err := maybeAddTagpending(p, hooks); err != nil {
		return err
	}

	if err := maybeAddKGB(p, hooks); err != nil {
		return err
	}

	return nil
}

func protectBranches(p *gitlab.Project) error {
	branches, _, err := salsa.ProtectedBranches.ListProtectedBranches(p.ID, &gitlab.ListProtectedBranchesOptions{})
	if err != nil {
		return err
	}
	toProtect := map[string]bool{
		"master":       true,
		"debian/*":     true,
		"upstream":     true,
		"upstream/*":   true,
		"pristine-tar": true,
	}
	for _, branch := range branches {
		delete(toProtect, branch.Name)
	}
	for branch := range toProtect {
		_, _, err := salsa.ProtectedBranches.ProtectRepositoryBranches(p.ID, &gitlab.ProtectRepositoryBranchesOptions{
			Name:             gitlab.String(branch),
			PushAccessLevel:  gitlab.AccessLevel(gitlab.DeveloperPermissions),
			MergeAccessLevel: gitlab.AccessLevel(gitlab.DeveloperPermissions),
		})
		if err != nil {
			return err
		}
	}
	return nil
}
