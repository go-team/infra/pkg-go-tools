package config

// After updating gitlabciymlTmpl, run salsa.debian.org/go-team/infra/pkg-go-tools/cmd/ci-config
const gitlabciymlTmpl = `# DO NOT MODIFY
# This file was automatically generated from the authoritative copy at:
# https://salsa.debian.org/go-team/infra/pkg-go-tools/blob/master/config/gitlabciyml.go
---
stages:
  - test
  - package

include:
  - project: go-team/infra/pkg-go-tools
    ref: master
    file: pipeline/test-archive.yml
    # Run the Go team CI only in the go-team project that has access to GitLab
    # CI runners tagged 'go-ci'
    rules:
      - if: $CI_PROJECT_ROOT_NAMESPACE  == "go-team"

Salsa CI:
  stage: package
  trigger:
    include:
      - project: salsa-ci-team/pipeline
        ref: master
        file: recipes/debian.yml
    strategy: depend
  rules:
    # Do not create a pipeline for tags unless SALSA_CI_ENABLE_PIPELINE_ON_TAGS is set
    - if: $CI_COMMIT_TAG != null && $SALSA_CI_ENABLE_PIPELINE_ON_TAGS !~ /^(1|yes|true)$/
      when: never
    # Avoid duplicated pipelines, do not run detached pipelines
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    # Run Salsa CI only if the Play button is pressed on the pipeline
    - if: $CI_PIPELINE_SOURCE == "push"
      when: manual
  variables:
    SALSA_CI_DISABLE_REPROTEST: 1 # Disable to save CI runner resources

# If Salsa CI is not running at
# https://salsa.debian.org/%{project_path}/-/pipelines, ensure that
# https://salsa.debian.org/%{project_path}/-/settings/ci_cd has in field "CI/CD
# configuration file" the same filename as this file.
#
# If Salsa CI is running, but first job is stuck because the project doesn't
# have any runners online assigned to it, ensure that
# https://salsa.debian.org/%{project_path}/-/settings/ci_cd has under "Runners"
# the setting for "Enable instance runners for this project" enabled.
`
