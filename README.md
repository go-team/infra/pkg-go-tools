# pkg-go-tools

This repository contains several tools used by the Go team infrastructure.

## ci-config

This tool is used to configure all repositories under the **go-team/packages** namespace and apply the configuration.

## ci-build

ci-build walks the $GOPATH created by pgt-gopath, examines the Debian packaging to figure out import paths to
build/test, and builds all packages in parallel.

When done, ci-build dumps the package build/test results to stdout.

## ci-diff

ci-diff compares two ci-build outputs. New breakages will be printed, existing breakages will be skipped.

## pgt-api-server

pgt-api-server exposes functionality for use by Debian go-team members. It is used by `dh-make-golang` for example when
running `create-salsa-project`.

## pgt-gopath

This program constructs a Go workspace src directory from the Debian unstable archive.

This eliminates the computationally intensive step of identifying reverse dependencies of Debian packages, and
eliminates the overhead of installing .deb packages, which is orders of magnitude slower than pgt-gopath.

## pgt-remote-add-upstream

This program configures an upstream git remote based on the `X-Vcs-Upstream-Git` value in debian/control,
or `XS-Go-Import-Path`, or a line matching “export DH_GOPKG := (.*)” in debian/rules.

It is intended to be used as a gbp postclone hook.